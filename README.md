GDBackUp
==============================

A set of scripts that you can run from the command line that will let you access Google Drive Service Accounts from the shell. Also supports delegated authority if you are a Google Apps administrator.

PLEASE NOTE:

These scripts will NOT let you access your personal Google Drive - they use a service account which is a special type of account designed for machine-to-machine use and can only be accessed programatically. They're useful for storing backups etc. and you get a 15GByte quota.

The only exception to this is if you are a Google Apps administrator - you can authorise the service account to access the user accounts of your Google Apps domain users and you can impersonate them, i.e. access their Drive as if you were them. Handy for programatically putting files in your users' Google Drives.

#### REQUIRED:

PHP 5.2+
PHP JSON extension
Google PHP SDK (a version is bundled).
HOW TO GET SET UP:

Access the Google API console at console.developers.google.com.
Select a project or create one.
Select APIs and Auth > APIs > Drive API and click the little cog.
Click API access > Create (another) Client ID.
Select Service Account > Create Client ID.
Download the .p12 file offered by Google and KEEP IT SAFE!
Note the "Client ID" and "E-Mail Address" displayed for the service account.
### SCRIPT CONFIGURATION:

Edit config/config.json
gd_client_id is the Client ID offered earlier.
gd_service_account_name is the E-Mail Address offered earlier.
gd_key_path is the path to the .p12 file downloaded earlier. Please note this is relative to the script's root directory (not the config directory).
If you are impersonating a delegated Google Apps account, gd_delegated_account is the account name to impersonate.
Otherwise, please leave it as null or the script will stop working.
### RUNNING THE SCRIPTS:

From the command line, call a sample script - e.g. php gdrive-info.php.
This should output the Google Drive quota information! Hurray!
You can do chmod a+x *.php if you want to be able to execute like a program, e.g. ./gdrive-info.php.
### HOW TO USE THE SCRIPTS:

Use the -h or --help switch on each script to get full information on parameters.

# CLI Usage (stored in cli directory)
## gdrive-download.php
Download a file from Google Drive

Usage:
> gdrive-download.php file_id save_path

file_id: The ID of the file to download, can be obtained from gdrive-listfiles.php.
save_path: The local filesystem folder to save the file to.

Notes

save_path does not accept UNIX shell shortcuts, e.g. ~. Use the absolute path.
save_path is mandatory; even if you want to download to the current working directory.
Explanation
Given a valid file ID and save_path, this will download the selected file from Google Drive to the save_path. The filename will be the title of the file in Google Drive. If the file to download is >1MB, the file will transfer in 1MB chunks; this will result in the file "appearing" on the filesystem almost as soon as the transfer starts, but it will not be complete until the script ends.

## gdrive-fileinfo.php
Get Info on a file in Google Drive

Usage:
> gdrive-fileinfo.php file_id

file_id: The ID of the file to get info on; can be obtained from gdrive-listfiles.php.

Explanation
Given a valid file_id, this script will print the file information for the selected file. Output includes the filename, ID, MD5-hash, MIME-type and size in bytes. It will also include links to the selected file (more info below), as well as the permissions for the file. If you attempt to run this command on a folder, not a file - only a subset of information will be displayed.

Links

Direct File URL is the link to the file's RAW data. The link requires a valid oAuth token.
Web Download Link is the direct Google "shared" link for people to download. This link respects permissions set on the file, e.g. if you have only shared the file with a certain other Google user, it will only let them (and you) download the file.
Viewer/Embed URL is the link to the file in the Google Drive interface.

## gdrive-info.php
Get Info on the current Google Drive user.

Usage:
> gdrive-info.php

Explanation
This will print the information on the current Google Drive user. This will most normally be the service account you specified in config/config.json, but if you are impersonating a Google Apps user (oAuth delegation), then it will be the account that you are impersonating.

Output includes the full name of the user and the quota bytes used and total.

## gdrive-listfiles.php
Lists files on Google Drive.

Usage:
> gdrive-fileinfo.php [folder_id]

[folder_id]: Optional ID of folder to list files under; if omitted then the root folder will be used.

Explanation
Use this script to get a list of items in a folder in Google Drive. If the script is called without any parameters, it will get the root Google Drive folder; if you call the script with a folder ID then it will get items directly underneath that folder ID. Using this script, you can navigate the hierarchy of your Google Drive.

Output
Each line will look like:

strv - ID - bytes - Title
Where the first 4 characters (strv) refer to whether the item is "Starred", "Trashed", "Restricted" or "Viewed", if the item does not have the flag, a - will be displayed instead, for instance ---v refers to a file that has been Viewed only.

## gdrive-upload.php
Upload a file to Google Drive

Usage:
> gdrive-upload.php file_to_upload [target_folder_id]

file_to_upload: The local file to upload, can be relative or absolute path.
[target_folder_id]: The target folder to upload the file to, if omitted will be the root Google Drive folder.

Explanation
Given a valid file_to_upload and optional target_folder_id; this script will upload your local file to Google Drive. If the target_folder_id is omitted; the file will be uploaded to the root Google Drive folder.

If the file to upload is >1MB, the file will be uploaded in chunks of 1MB, and a "." will be printed on the command line for each chunk - this is a crude way of allowing you to monitor the progress of the upload.