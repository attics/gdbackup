<?php
require_once 'google-api-php-client/src/Google_Client.php';
require_once 'google-api-php-client/src/contrib/Google_DriveService.php';


class GDBackUp {
	public $client;
	public $service;
	public $current_token;
	public $alltokens;
	protected $scope = array('https://www.googleapis.com/auth/drive');
	
	public function __construct($gd_client_id, $gd_service_account_name, $gd_key_path, $gd_delegated_account=null) {
	
		/**
		* Object constructor
		*
		* @param  string  $gd_client_id  The client ID for the application.
		* @param  string  $gd_service_account_name  The user-ID for the service account authorised.
		* @param  string  $gd_key_path  The path to the .p12 private key file associated with the service account.
		* @param  string  $gd_delegated_account  The account name to impersonate if you are using Google Apps domain-wide delegation.
		* @return void
		*/ 
		
		if (is_readable($gd_key_path)) {
			$this->client = new Google_Client();
			$this->client->setClientId($gd_client_id);
			$cred = new Google_AssertionCredentials(
				$gd_service_account_name,
				$this->scope,
				file_get_contents($gd_key_path)
			);
			
			if ($gd_delegated_account && $gd_delegated_account!="null") {
				$cred->sub = $gd_delegated_account;
			}
			
			$this->client->setAssertionCredentials($cred);
			$this->service = new Google_DriveService($this->client);
		} else {
			throw new Exception('Key file specified not found');
			exit;
		}
	
	}
	
	
	public function about() {
	
		/**
		* Get metadata about the currently logged in user.
		*
		* @return array User metadata contained in an associative array.
		*/ 
		try {
			$resp = $this->service->about->get();
		} catch (Exception $e) {
			throw $e;
		}
		return $resp;			
	}

	public function listFiles($parameters) {
	
		/**
		 * List files within a search scope on Google Drive
		 *
		 * @param  array  $parameters  An array of Google Drive search parameters.
		 * @return array  An associative array of files meeting the criteria.
		 */ 
		try {
			$resp = $this->service->files->listFiles($parameters);
			if (count($resp['items']) == 0) {
				throw new Exception('No files found under this parent. Folder may be empty or ID may not be a folder.');
				exit;
			}
		} catch (Exception $e) {
			throw $e;
			exit;
		}
		return $resp;
	}
	
	public function get($id) {
	
		/**
		* Get metadata on a file
		*
		* @param  array  $id  A Google Drive file ID reference.
		* @return array  An associative array file metadata.
		*/ 
		try {
		$resp = $this->service->files->get($id);
		} catch (Exception $e) {
			throw $e;
		}
		return $resp;
	}
	
	
	public function get_current_access_token() {
	
		/**
		* Get the current access token
		*
		* @return string  The currently valid access_token.
		*/ 
			
		$resp = $this->about();	
		return $this->current_token;
	}

	public function get_token_array() {
	
		/**
		* Get the current access token bundle, including access_token, refresh_token and expiry_time.
		*
		* @return array  The token bundle.
		*/ 
		
		$resp = $this->about();
		return $this->alltokens;	
	}
	
	public function list_permissions($id) {

		/**
		* Get permissions on a file
		*
		* @param  string  $id  A Google Drive file ID reference.
		* @return array  An associative array of file permissions.
		*/ 
	
		$resp = $this->service->permissions->listPermissions($id);
		return $resp;
	}
	
	public function upload($filename, $filesize, $filemime, $parentfolder) {
	
		/**
		* Upload a file
		*
		* @param  string  $filename  Name of the file to upload including full path.
		* @param  string  $filesize  Size of the file in bytes.
		* @param  string  $filemime  MIME content-type of the file.
		* @param  string  $parentfolder  A Google Drive ID referencing the folder to upload the file to.

		* @return string  The ID of the newly created file.
		*/ 
			
		$chunkSizeBytes = 1 * 1024 * 1024;
		$uf = $filename;
		
		$file = new Google_DriveFile();
		$file->setTitle(basename($filename));
		$file->setMimeType($filemime);

		$parent = new Google_ParentReference();
		$parent->setId($parentfolder);
		$file->setParents(array($parent));

		$media = new Google_MediaFileUpload($filemime, null, true, $chunkSizeBytes);
		$media->setFileSize($filesize);
		
		
		$result = $this->service->files->insert($file, array('mediaUpload' => $media));

		$status = false;
		$handle = fopen($uf, "rb");
	
		while (!$status && !feof($handle)) {
			$chunk = fread($handle, $chunkSizeBytes);
			$status = $media->nextChunk($result, $chunk);
			echo "."; // Echo one dot every chunk to indicate that it's working.
			ob_flush();
			flush();
		}
		
		$theresult = false;
		if ($status != false) {
			$theresult = $status;
		}

		fclose($handle);		
		return $theresult['id'];		
	}
	
	public function download($fileid, $savepath) {
	
		/**
		* Download a file
		*
		* @param  string  $fileid  Google Drive ID of the file to retrieve.
		* @param  string  $savepath  Folder on the filesystem to save the file to.

		* @return int  Number of bytes written to the filesystem.
		*/ 
		
		if (substr($savepath, -1) != "/") {
			$savepath = $savepath."/";
		}
		

		if (!is_writable($savepath)) {
			throw new Exception('Path does not exist or is not writable');
			exit;
		}
		
		try {
			$metadata = $this->get($fileid);
		} catch (Exception $e) {
			throw $e;
			exit;
		}
		
				
		$signed = new Google_HttpRequest($metadata['downloadUrl'], 'GET', null, null);
		$this->client->getAuth()->sign($signed);
		$signedparams = $signed->getRequestHeaders();
		$signedparamsauth = $signedparams['authorization'];
		$opts = array('http' =>
			array(
				'method' => "GET",
				'header' => "Authorization: ".$signedparamsauth
			)
		);
		
		$context = stream_context_create($opts);
		
		$authenticatedFilePath = $metadata['downloadUrl'];
		$concatenatedSavePath = $savepath.$metadata['title'];

		$chunksize = 1 * (1024 * 1024); 
		$bytesWritten = 0;
		if ($metadata['fileSize'] > $chunksize) { 
			$handle = fopen($authenticatedFilePath, 'r', false, $context);
			$savehandle = fopen($concatenatedSavePath, "wb");
			$buffer = ''; 
			while (!feof($handle)) { 
				$buffer = fread($handle, $chunksize); 
				$bytesWritten += fwrite($savehandle, $buffer);
				ob_flush(); 
				flush(); 
			} 
			fclose($handle); 
			fclose($savehandle);
		} else { 
			$bytesWritten = file_put_contents($concatenatedSavePath, file_get_contents($authenticatedFilePath, false, $context));
		}
		
		if ($bytesWritten != 0) {
			return $bytesWritten;
		} else {
			throw new Exception('No bytes saved - error saving file?');
			exit;
		}

	}
	
	protected function common_code() {
	
		$auth = $this->client->getAuth();
	
		$tokenarray = array();
		$tokenarray = $auth->token;			
	
		$tsresp = gd_tokenstore::save_tokens_to_store($tokenarray);								
		$this->current_token = $tokenarray['access_token'];
		$this->alltokens = $tokenarray;	
		
	}	


}
?>