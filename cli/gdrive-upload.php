#!/usr/bin/php
<?php

ob_implicit_flush(1);
set_time_limit(0);

if ($_SERVER['argc'] < 2 || $_SERVER['argc'] > 3 || in_array('-h', $_SERVER['argv']) || in_array('--help', $_SERVER['argv'])) {
	echo "Usage: gdrive-upload.php file_to_upload [target_folder_id]\n";
	echo "file_to_upload: path to the file to upload.\n";
	echo "target_folder_id: Google ID of the folder that will contain the file. If omitted, will be the root Google Drive folder.\n";
	exit(1);
}

$ulfile = $argv[1];
$mime = mime_content_type($ulfile);
$filesize = filesize($ulfile);

require_once __DIR__ . "/../src/GDBackUp.php";
$configuration = json_decode(file_get_contents(__DIR__ . '/../config/config.json'), true);

if ($configuration) {

	$gd = new GDBackUp($configuration['gd_client_id'], $configuration['gd_service_account_name'], $configuration['gd_key_path'], $configuration['gd_delegated_account']);
	$target = 'root';
	if ($_SERVER['argc'] == 3) {
		$target = $_SERVER['argv'][2];
	}
	$resp = $gd->upload($ulfile, $filesize, $mime, $target);
	
	echo "\nFile transfer complete\n";
	printf("ID of new file is: %s\n", $resp);

} else {
	echo "Error loading configuration\n";
	exit(1);
}
?>