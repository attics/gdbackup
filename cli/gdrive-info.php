#!/usr/bin/php
<?php
require_once __DIR__ . '../src/GDBackUp.php';
$configuration = json_decode(file_get_contents(__DIR__ . '/../config/config.json'), true);

if ($configuration) {

	$gd = new GDBackUp($configuration['gd_client_id'], $configuration['gd_service_account_name'], $configuration['gd_key_path'], $configuration['gd_delegated_account']);
	try {
		$about = $gd->about();	
		printf("Currently logged in as: %s\n", $about['name']);
		printf("Quota bytes used: %s\n", (string)$about['quotaBytesUsed']);
		printf("Quota bytes total: %s\n", (string)$about['quotaBytesTotal']);
		printf("Quota type: %s\n", (string)$about['quotaType']);
	} catch (Exception $e) {
		echo $e->getMessage()."\n";
		exit(1);
	}
} else {
	echo "Error loading configuration\n";
	exit(1);
}
?>